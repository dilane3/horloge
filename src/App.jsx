import React, {useState} from 'react';
import {Modal, Button} from 'react-bootstrap';
import "./horloge.css";

const fixedDate = new Date();
const fixedHours = fixedDate.getHours() * 3600000;
const fixedMinutes = fixedDate.getMinutes() * 60000;

const ConfigModal = ({show, setClose, hour, minutes, date, setDate, setMinutes, setHours, reset}) => {
  const [h, setH] = useState(hour);
  const [m, setM] = useState(minutes);
  const [d, setD] = useState(date);
  const [dTime, setDTime] = useState(date.getTime());

  React.useEffect(() => {
    setH(hour);
    setM(minutes);
    setD(date);
  }, [hour, minutes, date]);

  const changeDate = (value) => {
    const dateTime = new Date(new Date(value).getTime() - 3600000);

    setD(value);
    setDTime(dateTime);
  }

  const handleChangeDate = () => {
    console.log(h);
    console.log(m);
    console.log(new Date(d).toString());
    console.log(new Date(d).toUTCString());

    setMinutes(m);
    setHours(h);
    setDate(new Date(dTime));
  }

  return (
    <Modal show={show} onHide={setClose}>
      <Modal.Body>
        <div className="config-date" style={{width: "100%"}}>
          <label htmlFor="date">Date: </label>
          <input className="form-control" value={d} onChange={event => changeDate(event.target.value)} type="date" />
        </div>
        <div className="config-hours" style={{width: "100%"}}>
          <label className="mt-2" htmlFor="heure">Heure: </label>
          <select className="form-control" value={h} onChange={event => setH(Number(event.target.value))}>
            {
              Array(24).fill(1).map((val, index) => <option value={index*3600000}>{`${index < 10 ? '0'+index:index}h`}</option>)
            }
          </select>
        </div>
        <div className="config-minutes" style={{width: "100%"}}>
          <label className="mt-2" htmlFor="minutes">Minutes: </label>
          <select className="form-control" value={m} onChange={event => setM(Number(event.target.value))}>
            {
              Array(60).fill(1).map((val, index) => <option value={index*60000}>{`${index < 10 ? '0'+index:index}min`}</option>)
            }
          </select>
        </div>
      </Modal.Body>
      <Modal.Footer>

        <div className="d-flex flex-row justify-content-between" style={{width: "100%"}}>
          <Button variant="danger" onClick={() => {reset(); setClose()}}>Reinitialiser</Button>
          <div className="d-flex flex-row justify-content-end button-config" style={{width: "60%"}}>
            <Button variant="light" onClick={setClose}>fermer</Button>
            <Button variant="primary" onClick={() => {handleChangeDate(); setClose()}}>valider</Button>
          </div>
        </div>
      </Modal.Footer>
    </Modal>
  )
}

const App = () => {
  const [date, setDate] = useState(new Date(fixedDate.getTime() - fixedHours - fixedMinutes));
  const [hour, setHour] = useState(fixedHours);
  const [minutes, setMinutes] = useState(fixedMinutes);
  const [currentDate, setCurrentDate] = useState(new Date());
  const [show, setShow] = useState(false);

  React.useEffect(() => {
    let time = setInterval(() => {
      setCurrentDate(new Date());
    }, 1000)

    return () => {
      clearInterval(time);
    }
  }, [currentDate, hour]);

  // definition des fontions
  const displayDate = () => {
    const correctDate = new Date(currentDate.getTime() - fixedDate.getTime() + (date.getTime() + hour + minutes));

    let year = correctDate.getFullYear();
    let month = getStringMonth(correctDate.getMonth());
    let day = getStringDay(correctDate.getDay());
    let dat = correctDate.getDate();

    return {date:dat, day, month, year};
  }

  const displayTime = () => {
    const correctDate = new Date(currentDate.getTime() - fixedDate.getTime() + (date.getTime() + hour + minutes));

    let h = correctDate.getHours();
    let m = correctDate.getMinutes();
    let seconds = correctDate.getSeconds();

    return {hour:h, minutes:m, seconds};
  }

  const reset = () => {
    const cDate = currentDate;
    const hour = cDate.getHours() * 3600000;
    const minutes = cDate.getMinutes() * 60000;

    setHour(hour);
    setMinutes(minutes);
    setDate(new Date(cDate.getTime() - hour - minutes));
  }

  const getStringMonth = (month) => {
    switch (month) {
      case 0:
        return "Janvier";
      case 1:
        return "Fevrier";
      case 2:
        return "Mars";
      case 3:
        return "Avril";
      case 4:
        return "Mai";
      case 5:
        return "Juin";
      case 6:
        return "Juillet";
      case 7:
        return "Aout";
      case 8:
        return "Septembre";
      case 9:
        return "Octobre";
      case 10:
        return "Novembre";
      case 11:
        return "Decembre";
      default:
        // nothing
        break;
    }
  }

  const getStringDay = (day) => {
    switch (day) {
      case 0:
        return "Dimanche";
      case 1:
        return "Lundi";
      case 2:
        return "Mardi";
      case 3:
        return "Mercredi";
      case 4:
        return "Jeudi";
      case 5:
        return "Vendredi";
      case 6:
        return "Samedi";
      default:
        // nothing
        break;
    }
  }

  const getPeriod = () => {
    let h = hour / 3600000;

    if (h < 12) {
      return "MATIN";
    } else if (h === 12) {
      return "MIDI";
    } else if (h > 12 && h < 14) {
      return "APRES MIDI";
    }

    return "SOIR";
  }

  return (
    <section className="container">
      <article className="horloge-box">
        <div className="horloge">
          <div className="horloge-screen">
            <span className="horloge-day">{displayDate().day.toUpperCase()}</span>
            <span className="horloge-period">{getPeriod()}</span>
            <span className="horloge-time">{`${displayTime().hour < 10 ? '0'+displayTime().hour:displayTime().hour}:${displayTime().minutes < 10? '0'+displayTime().minutes:displayTime().minutes}:${displayTime().seconds < 10? '0'+displayTime().seconds:displayTime().seconds}`}</span>
            <span className="horloge-date">{`${displayDate().date} ${displayDate().month.toUpperCase()} ${displayDate().year}`}</span>
          </div>
        </div>
        <div>
          <div className="horloge-config">
            <button className="btn btn-config" onClick={() => setShow(true)}>configuration</button>
          </div>
          <div className="horloge-design"></div>
        </div>
      </article>

      <ConfigModal
        show={show}
        setClose={() => setShow(false)}
        hour={hour}
        minutes={minutes}
        setHours={setHour}
        setMinutes={setMinutes}
        date={date}
        setDate={setDate}
        reset={reset}
      />
    </section>
  )
};

export default App;
